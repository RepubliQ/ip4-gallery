<?php

namespace Plugin\Req_Gallery;

use Ip\Response\Json;

class AdminController extends \Ip\GridController
{
    /**
     * Ajax call from "Add Images" button.
     * @return \Ip\Response\Json
     */
    public static function saveImages()
    {
        if (!ipIsManagementState() || !ipRequest()->isPost()) return new Json(array('status' => 'error', 'message' => 'Conditions for the call not met.'));
        $data = ipRequest()->getPost();
        return Model::saveImages($data['gallery'], json_decode($data['images']));
    }

    /**
     * @return array
     */
    protected function config()
    {
        return array(
            'title' => 'Gallery',
            'table' => 'req_gallery',
            'deleteWarning' => 'Are you sure?',
            'createPosition' => 'top',
            'fields' => array(
                array(
                    'type' => 'Text',
                    'label' => 'Title',
                    'field' => 'title',
                    'validators' => array('Required')
                ),
                array(
                    'type' => 'Grid',
                    'label' => 'Images',
                    'field' => 'gallery_id',
                    'config' => array(
                        'sortField' => 'image_order',
                        'title' => 'Images',
                        'connectionField' => 'gallery_id',
                        'table' => 'req_gallery_images',
                        'deleteWarning' => 'Are you sure?',
                        'allowCreate' => false,
                        'allowSearch' => false,
                        'fields' => array(
                            array(
                                'type' => 'Text',
                                'label' => 'Title',
                                'field' => 'title'
                            ),
                            array(
                                'type' => 'Textarea',
                                'label' => 'Description',
                                'field' => 'description'
                            ),
                            array(
                                'type' => 'RepositoryFile',
                                'label' => 'Image',
                                'field' => 'image',
                                'fileLimit' => 1,
                                'preview' => 'Plugin\Req_Gallery\Model::gridImageThumbnail'
                            )
                        ),
                        'actions' => array(
                            array(
                                'label' => 'Add Images',
                                'class' => 'addImagesButton',
                                'data' => ''
                            )
                        )
                    )
                )
            )
        );
    }
}