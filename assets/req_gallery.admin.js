$('.ipsGrid').on('init.ipGrid', function () {
    $('.addImagesButton').on('click', function (e) {
        ipBrowseFile(function (files) {
            var address = document.location.hash.substring(1).split('&');
            var addressLength = address.length;
            var galleryId;

            for (var i = 0; i < addressLength; i++) {
                var pair = address[i].split('=');

                if (pair[0] == 'gridParentId1') {
                    galleryId = pair[1];
                    break;
                }
            }

            $.ajax({
                url: '/?aa=Req_Gallery.saveImages',
                type: 'post',
                dataType: 'json',
                data: {
                    securityToken: ip.securityToken,
                    gallery: galleryId + '',
                    images: JSON.stringify(files)
                },
                success: function (response) {
                    if (response.status == 'ok') {
                        $('.ipsGrid').ipGrid('refresh');
                    } else {
                        alert(response.message);
                    }
                }
            });
        }, {preview: 'thumbnails', filter: 'images'});
    })
});