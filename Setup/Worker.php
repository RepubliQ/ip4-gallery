<?php

namespace Plugin\Req_Gallery\Setup;

class Worker extends \Ip\SetupWorker
{
    public function activate()
    {
        // Galleries
        $table = ipTable('req_gallery');
        $sql =
            "CREATE TABLE IF NOT EXISTS $table (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255),
              PRIMARY KEY (`id`)
            )";
        ipDb()->execute($sql);

        // Images
        $table = ipTable('req_gallery_images');
        $sql =
            "CREATE TABLE IF NOT EXISTS $table (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `gallery_id` INT(11),
              `image_order` DOUBLE DEFAULT 1,
              `title` VARCHAR(255),
              `description` TEXT,
              `image` VARCHAR(255),
              `preview` VARCHAR(255),
              PRIMARY KEY (`id`)
            )";
        ipDb()->execute($sql);
    }

    public function deactivate()
    {

    }

    public function remove()
    {

    }

}