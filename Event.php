<?php

namespace Plugin\Req_Gallery;

class Event
{
    public static function ipBeforeController()
    {
        if (ipIsManagementState()) {
            ipAddJs('assets/req_gallery.admin.js');
        }
    }
}