<?php

namespace Plugin\Req_Gallery\Entity;

use Plugin\Req_Gallery\Model;

class Gallery
{
    /** @var int */
    public $id;

    /** @var string */
    public $title;

    function __construct($data = null)
    {
        if ($data != null) {
            $this->id = $data['id'];
            $this->title = $data['title'];
        }
    }

    /**
     * Returns list of images belonging to this gallery.
     * @param bool $asIterator return
     * @param string|array $cols Columns.
     * @param bool|int|array $limit Limit.
     * @return null|Image[]|\Plugin\Req_Gallery\Iterator
     */
    function getImages($asIterator = false, $cols = array(), $limit = false)
    {
        return Model::getImages(intval($this->id), $asIterator, $cols, $limit);
    }
}