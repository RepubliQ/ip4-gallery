<?php

namespace Plugin\Req_Gallery\Entity;

use Plugin\Req_Gallery\Model;

class Image
{
    /** @var string */
    public $id;

    /** @var int */
    public $gallery_id;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string */
    public $image;

    /** @var string */
    public $preview;

    function __construct($data = null)
    {
        if ($data != null) {
            $this->id = $data['id'];
            $this->gallery_id = $data['gallery_id'];
            $this->image = $data['image'];
            $this->preview = $data['preview'];
            $this->title = $data['title'];
            $this->description = $data['description'];
        }
    }

    /**
     * Retrieves the parent Gallery object.
     * @return null|Gallery
     */
    public function getGallery()
    {
        if (!isset($this->id)) return null;
        return Model::getGallery(intval($this->gallery_id));
    }

    /**
     * Returns gallery id.
     * @param bool $fetch Read the value from database if not present.
     * @return int|null|string
     */
    public function getGalleryId($fetch = false) {
        if (isset($this->gallery_id)) return $this->gallery_id;

        if ($fetch) {
            $table = ipTable('req_gallery_images');
            return ipDb()->fetchValue("SELECT `gallery_id` FROM $table WHERE `id` = {$this->id}");
        }

        return null;
    }

    /**
     * Returns title.
     * @param bool $fetch Read the value from database if not present.
     * @return int|null|string
     */
    public function getTitle($fetch = false) {
        if (isset($this->title)) return $this->title;

        if ($fetch) {
            $table = ipTable('req_gallery_images');
            return ipDb()->fetchValue("SELECT `title` FROM $table WHERE `id` = {$this->id}");
        }

        return null;
    }

    /**
     * Returns description.
     * @param bool $fetch Read the value from database if not present.
     * @return int|null|string
     */
    public function getDescription($fetch = false) {
        if (isset($this->description)) return $this->description;

        if ($fetch) {
            $table = ipTable('req_gallery_images');
            return ipDb()->fetchValue("SELECT `description` FROM $table WHERE `id` = {$this->id}");
        }

        return null;
    }

    /**
     * Returns image.
     * @param bool $fetch Read the value from database if not present.
     * @return int|null|string
     */
    public function getImage($fetch = false) {
        if (isset($this->image)) return $this->image;

        if ($fetch) {
            $table = ipTable('req_gallery_images');
            return ipDb()->fetchValue("SELECT `image` FROM $table WHERE `id` = {$this->id}");
        }

        return null;
    }

    /**
     * Returns preview.
     * @param bool $fetch Read the value from database if not present.
     * @return int|null|string
     */
    public function getPreview($fetch = false) {
        if (isset($this->preview)) return $this->preview;

        if ($fetch) {
            $table = ipTable('req_gallery_images');
            return ipDb()->fetchValue("SELECT `preview` FROM $table WHERE `id` = {$this->id}");
        }

        return null;
    }
}