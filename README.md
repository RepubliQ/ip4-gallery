## Req_Gallery Plugin ##

Admin side gallery with the ability to select and save multiple images at one. A separate database entry is created for each image.

### Features ###

- Select multiple images at once;
- IDE targeted;
- Object oriented approach.

### Downfalls ###

- No view output, widget or slot.

### Database ###

Galley: `id`, `title`.

Images: `id`, `gallery_id`, `image_order`, `title`, `description`, `image`, `preview`.

### Usage ###

An `Iterator` is provided for each list retrieving function for extremely large galleries. It allows to read the entries one by one instead of saving all the entries to memory.

**Available functions**: 

- `\Plugin\Req_Gallery\Model::getGalleries($asIterator = false)` - get all galleries.
- `\Plugin\Req_Gallery\Model::getGallery($id)` - get specific gallery.
- `\Plugin\Req_Gallery\Model::getImage($id)` - get specific image.
- `\Plugin\Req_Gallery\Model::getImages($galleryId, $asIterator = false, $cols = array(), $limit = false)` - get specific gallery images.

Each returned object properties can be accessed directly:

> $gallery = \Plugin\Req_Gallery\Model::getGallery(1);

> $gallery->title;

They also provide methods to get images or parent gallery:

> $gallery->getImages($asIterator = false, $cols = array(), $limit = false)

> $image->getGallery()

When you retrieved specific columns for `Images` e.g.:

> $gallery = \Plugin\Req_Gallery\Model::getImages(1, false, 'title');

or

> $gallery = \Plugin\Req_Gallery\Model::getImages(1, false, array('title', 'image'));

and somewhere in the loop you need description for specific `Image`, you can use the provided `get` methods, e.g.:

> getDescription($fetch = false)

and pass `true` as a parameter. What happens is, if the `description` is not set, it is retrieved from the database. If the parameter is false, `null` is returned if the `description` is not set.