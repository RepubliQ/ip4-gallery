<?php

namespace Plugin\Req_Gallery;

use Ip\Exception;

class Iterator implements \Countable, \Iterator
{
    private $sql;
    private $params;
    private $class;

    /** @var \PDOStatement */
    private $recordSet;
    private $cursor;
    private $item;

    function __construct($sql, $class, $params = array())
    {
        if (!is_string($class)) throw new Exception("Class must be defined.");

        $this->sql = $sql;
        $this->class = $class;
        $this->params = $params;
        $this->reset();
    }

    private function reset()
    {
        $this->recordSet = ipDb()->getConnection()->prepare($this->sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        foreach ($this->params as $key => $value) {
            if (is_bool($value)) {
                $value = $value ? 1 : 0;
            }
            $this->recordSet->bindValue(is_numeric($key) ? $key + 1 : $key, $value);
        }
        $this->recordSet->setFetchMode(\PDO::FETCH_CLASS, $this->class);
        $this->recordSet->execute();

        $this->cursor = -1;
        $this->item = null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->item;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->cursor;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return ($this->cursor < $this->count());
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return $this->recordSet->rowCount();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if ($this->cursor >= 0) $this->reset();
        $this->next();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->cursor++;
        $this->item = $this->recordSet->fetch(\PDO::FETCH_ORI_NEXT);
    }
}