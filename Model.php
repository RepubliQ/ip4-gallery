<?php

namespace Plugin\Req_Gallery;

use Ip\Response\Json;

class Model
{
    /**
     * Get a list of all galleries.
     * @param bool $asIterator Return as Iterator.
     * @return Entity\Gallery[]|Iterator
     */
    public static function getGalleries($asIterator = false)
    {
        $table = ipTable('req_gallery');
        $sql = "SELECT * FROM $table";

        if ($asIterator) {
            return new Iterator($sql, '\Plugin\Req_Gallery\Entity\Gallery');
        } else {
            $query = ipDb()->getConnection()->prepare($sql);
            $query->execute();

            return $query->fetchAll(\PDO::FETCH_CLASS, '\Plugin\Req_Gallery\Entity\Gallery');
        }
    }

    /**
     * Retrieves a gallery object.
     * @param $id int Gallery id.
     * @return Entity\Gallery|null
     */
    public static function getGallery($id)
    {
        if (!is_integer($id)) return null;

        $table = ipTable('req_gallery');
        $sql = "SELECT * FROM $table WHERE `id` = '$id' LIMIT 1";
        $query = ipDb()->getConnection()->prepare($sql);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_CLASS, '\Plugin\Req_Gallery\Entity\Gallery');
        return $result ? $result[0] : null;
    }

    /**
     * Retrieves a gallery object.
     * @param $id int Gallery id.
     * @return Entity\Image|null
     */
    public static function getImage($id)
    {
        if (!is_integer($id)) return null;

        $table = ipTable('req_gallery_images');
        $sql = "SELECT * FROM $table WHERE `id` = '$id' LIMIT 1";
        $query = ipDb()->getConnection()->prepare($sql);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_CLASS, '\Plugin\Req_Gallery\Entity\Image');
        return $result ? $result[0] : null;
    }

    /**
     * Retrieves all images from gallery.
     * @param $galleryId int gallery id.
     * @param bool $asIterator Return as Iterator.
     * @param array|string $cols Columns.
     * @param bool|int|array $limit Limit result count.
     * @return Entity\Image[]|Iterator|null
     */
    public static function getImages($galleryId, $asIterator = false, $cols = array(), $limit = false)
    {
        if (!is_integer($galleryId) && !is_bool($asIterator)) return null;

        if (is_string($cols)) {
            if (strtolower($cols) != 'id') $columns = "`id`, `$cols`";
            else $columns = "`$cols`";
        } else if (is_array($cols) && $cols) {
            if (!isset($cols['id'])) $cols[] = 'id';
            $columns = '`' . implode('`, `', $cols) . '`';
        } else $columns = "*";

        if (is_integer($limit)) $lmt = "LIMIT $limit";
        else if (is_array($limit) && count($limit) == 2 && is_integer($limit[0] + $limit[1])) $lmt = "LIMIT {$limit[0]}, {$limit[1]}";
        else $lmt = "";

        $table = ipTable('req_gallery_images');
        $sql = "SELECT $columns FROM $table WHERE `gallery_id` = '$galleryId' ORDER BY `image_order` ASC $lmt";

        if ($asIterator) {
            return new Iterator($sql, '\Plugin\Req_Gallery\Entity\Image');
        } else {
            $query = ipDb()->getConnection()->prepare($sql);
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_CLASS, '\Plugin\Req_Gallery\Entity\Image');
        }
    }

    /**
     * Show image thumbnail in grid controller.
     * @param null $value
     * @param $recordData array Grid element array data.
     * @return string HTML.
     */
    public static function gridImageThumbnail($value = null, $recordData)
    {
        return $value ? '<img style="width: 100px;" src="' . $recordData['preview'] . '" alt="" />' : '';
    }

    /**
     * Saves all images to database sent by ajax.
     * @param $galleryId int Gallery ID.
     * @param $images array Images array.
     * @return Json
     */
    public static function saveImages($galleryId, $images)
    {
        if (!is_string($galleryId) || !is_array($images) || !ipRequest()->isPost()) return new Json(array('status' => 'error', 'message' => 'Invalid argument or call.'));
        if (count($images) == 0) return new Json(array('status' => 'ok'));

        foreach ($images as $image) {
            ipDb()->insert('req_gallery_images', array('gallery_id' => $galleryId, 'image' => $image->fileName, 'preview' => $image->previewUrl));
        }

        return new Json(array('status' => 'ok'));
    }
}